# Administración correcta Linux


Este curso se baso en el antiguo libro de administración 
de linux de McGraw-Hill, el cual contenía muchos errores 
que se corrigieron y se fusiono con otros cursos de `blacktrack`.

En materia de seguridad, `Offensive-Security` es muy popular, 
y las herramientas mas usadas son el combo de `metaxploit` de 
la misma web donde se fabrica el windoserisimo kali linux.

Este curso solo **se centrara en corregir las malas practicas**, 
para un enfoque en seguridad por ofuscación se debe preguntar 
donde siempre en el chat https://t.me/latam_programadores 
ya que **las malas practicas conllevan a mala seguridad**.

## Objetivos y aclaratorias

Un **hacker** no es el que rompe y "hackea" el sistema sino el 
desarrollador, conocedor y sabiondo, aclaremos la ignorancia.

Un **cracker** es ese que rompe, penetra y daña, es el que se 
trata de evitar, y **lammer** es aquel que intenta ser como los 
anteriores, pero solo sabe repetir lo que hacen y fabrican otros.

**Este curso solo le da las herramientas y si ud no tiene comprensión 
lectora, olvídese que solucionara los problemas, aquí se le dice como 
debe hacerse las cosas en un principio.**

## Contenido

* Entorno e instalación  - 3 hrs
  * Ajuste de requisitos según el hardware
  * Estandarización y estructura del disco.
  * Estandarización del usuario.
  * Estandarización de archivos y rutas
  * Vías de instalación externas

* Redes básicas - 9 hrs
  * Capas OSI, puertos, servicios
  * Puertos y mecanismos - HTTP/FTP/SMTP/DNS
  * Servicios en linux, mecanismos de procesos
  * Intranets y extranets, ipv4, ipv6
  * El SSH y sus debilidades

* Entorno web  - 9 hrs
  * LAMP empleando lighttpd porque es el correcto
  * LAMP empleando nginx dado su popularidad
  * LAMP ajustado los repos de terceros
  * Conceptos de cliente y servidor
  * CGI/NODE/SCRIPT empleando repos terceros

* Entorno desarrollo: - 9 hrs
  * Estandarización de archivos y rutas (si no se vio)
  * El sistema de archivos PROC y TMPFS
  * Usuarios y ejecución
  * CHROOT y DOCKERS
  * VIM vs GEANY, configuración de desarrollo
  * GIT versionado de archivos

* Entorno de producción - 9 hrs
  * deploys vs devcops
  * El entorno de producción
  * Servidores vs servicios


**Acerca de las horas**: el máximo de cada modulo es 9 horas, y 
con una hora gratis de consulta. El primer modulo solo tiene 3 horas, y 
las horas gratis solo aplican a los otros módulos, el primer modulo 
define la continuidad de los demás módulos,

**Acerca de horas gratis**: no son para los contenidos, sino para las practicas y 
correcciones, esto es porque cada participante necesita realizar la practica 
de manera supervisada, pro ende esta hora se dará gratis, menos el primer modulo 
ya que este primero necesita una parte practica siempre.

**Acerca de los horarios** cada modulo requerirá una semana, las horas 
se repartirán en dicha semana, al terminar un modulo la siguiente clase 
es la clase gratis y es únicamente sobre el modulo terminado, después de 
esta, una semana libre de preparación para el siguiente modulo.


## Detalles de contenidos

Los títulos son los correctos mas no los deseados, aquí el detalle 
del porque y que significan:

#### Entorno e instalación

Con lo fácil que es usar linux hoy se olvida que **estos facilitamos 
no están diseñados para el desarrollo y mucho menos para despliegues 
en servidores**.

Entre los linux mas usados para servicios esta Red Hat y Suse, y esto es 
por su alto nivel de estandarización, sin embargo dado están orientado a 
facilitar las cosas, sus requerimientos son altos (son lentos) y están ""abultados" 
siendo en su mayoría ineficientes para altas prestaciones.

En este capítulo se definirá un usuario estándar en sesiones remotas 
(que el cursante junto a grupo definirán), y otro usuario estándar 
(que no se podrá cambiar, pero inaccesible en servers) para el trabajo.

Por ultimo se darán vías alternas de instalación que no requieren que el 
computador o servidor este presente.

#### Redes básicas

Esto es que los programadores y desarrolladores dirán que ya saben de redes, 
pero sucede que un **programador con conceptos errados de redes es un libro 
abierto al mundo del crak**.

Hoy día muchos servicios no requieren ""un pedido de algo"" para enviar 
información, lo que deja a que el mismo desarrollador es al mismo tiempo 
su propio verdugo si no sabe o tiene una visión abstracta de lo que pasara.

#### Entorno web

LAMP se ha vuelto popular pero **ha embrutecido el desarrollo, cayendo todo 
en entornos de baja seguridad**. En este capitulo se enseñara a definir el entorno 
correcto tanto de producción como de desarrollo.

Adicional **se actualizara la vieja filosofía "pido y obtengo"**, ya que en el mundo 
moderno esta filosofía (llamada petición a respuesta) **ahora soporta 
el no tener que esperar por una respuesta**, sino que esta se obtiene después, 
a disponibilidad, lo que evidentemente complica la seguridad si no se entiende.

En este curso se deberá como se menciono seguir estándares, pero no siempre 
estos están alineados a las necesidades, para ello se enseñara algunas vías 
extras de como obtener paquetes alineados con la distribución de uso Debian.

#### Entorno desarrollo

En este modulo se realizaran estándares para poder tener un entorno conocido, 
entre los desarrolladores, el conocer mas ayuda a identificar mas.

Esto es para discernir un error de un problema de compatibilidad entre desarrollos.

Se debe saber donde colocar los archivos, como trabajar con `git`, aplicar 
cambios de manera rápida, establecer reglas de cambios y mejoras, así 
como reglas de contribuciones cuando hay muchos proyectos en juego. Aquí 
el sistema `git` es la punta de lanza y un centro de trabajo es imprescindible.

Por ultimo, las herramientas de uso deben estar homologadas, indistintas, 
porque cada versión puede contener diferencias llamadas "mejoras" que no 
serán compatibles, dificultando el trabajo de desarrollo.

#### Entorno de producción

Este es la prueba final, estos entornos son limitados en herramientas, 
además tienen limitantes de lado de la red (firewall) que fuerzan ejecutarse 
de maneras especificas.

Si no se definen reglas correctas se obtendrán dos escenarios:
* un despliegue exitoso pero inseguro
* un despliegue fallido dado no se distingue un error de una limitación.

## Preguntas comunes

#### No entiendo "XYZ", ¿qué debo hacer?

Se espera y se requiere un grado de comprensión lectora, los problemas 
ocurren porque las cosas se hacen sin pensar.

el decir "que debo hacer" se traduce como "NO SE QUE HACER DIME TU QUE SABES", 
si esto es así, ud esta en el lugar equivocado, capacitados los hay pocos, 
y preguntones están sobrando, estos los atacantes "crakers" también 
solo saben preguntar cuando tienen problemas.

Si no está familiarizado con un determinado tema, debe dedicar tiempo 
a la auto-investigación sobre el problema antes de intentar mas. 

Solo para los autodidactas no hay nada más satisfactorio que resolver un 
problema sin depender de los demás  PERO NO CONFUNDA ESO CON "solucionar rápido".

#### No tengo tiempo, estoy ocupado con otras cosas!

En ese caso u contrate a un experto como los que damos este curso, 
no le hablaremos de mentiras ni rodearemos el tema, así que deje 
a los expertos trabajar!

RECUERDE: no se le dice al que sabe que va hacer, se supone el esta allí 
para decirle que debe hacer, porque para eso ud lo busco, que es el que no sabe.

En el caso que ud no tiene la capacidad ni el interés y solo desea el resultado?
deberá contratar a la persona correcta, pagar menos es costo mayor en el futuro.
